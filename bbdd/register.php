<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registro</title>

  <!-- Custom fonts for this template-->

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">¡Crea una cuenta!</h1>
              </div>
              <form action="conexiones/registroU.php" method="post" class="user">
                <div class="form-group row">
				  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="nick" name="nick" placeholder="Nick:" required>
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre:" required>
                  </div>
				   </div>
				  <div class="form-group row">
				  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="apellido" name="apellido" placeholder="Apellidos:" required>
					</div>
				  </div>
				<div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="int" class="form-control form-control-user" id="edad" name="edad" placeholder="Edad:" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="int" class="form-control form-control-user" id="telefono" name="telefono" placeholder="Telefono:" minlength="9" maxlength="9" required>
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Email:" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="contrasena" name="contrasena" placeholder="Contraseña:" minlength="6" required>
                  </div>
				  </div>
				<button type="submit" name="submit" class="btn btn-primary btn-user btn-block">
                  Registrarse
                </button>
                <hr>
               
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="forgot-password.html">¿Has olvidado tu contraseña?</a>
              </div>
              <div class="text-center">
                <a class="small" href="login.html">¿Ya tienes una cuenta? ¡Inicia sesión!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</body>

</html>


