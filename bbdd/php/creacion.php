<?php
   
	$hostname = 'localhost';
	$database = 'pisos';
	$username = 'root';
	$password = '';

	$conexion = new mysqli($hostname, $database, $username, $password);

    if($conexion->connect_error){
        die("Conexión fallida: " . $conexion->connect_error);
    }

    /*
    $sql = "CREATE DATABASE todolistDB";
    if($conexion->query($sql) === true){
        echo "Base de datos creada correctamente...";
    }else{
        die("Error al crear base de datos: " . $conexion->error);
    }
    */

    $sql = "CREATE TABLE edificios(
		Titulo	varchar(50)	utf16_spanish2_ci NOT NULL,
		N_Habitaciones	int(2) NOT NULL,
		Precio	int(6) NOT NULL,
		Descripcion	varchar(250) utf16_spanish2_ci NOT NULL,
		Distancia int(8) NOT NULL,
		Telefono int(9) NOT NULL
        
    )";

    if($conexion->query($sql) === true){
        echo "La tabla se creó correctamente...";
    }else{
        die("Error al crear tabla: " . $conexion->error);
    }

?>